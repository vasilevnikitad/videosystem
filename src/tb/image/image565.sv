`ifndef __IMAGE565_SV
`define __IMAGE565_SV

`include "image.svh"
class Image565 extends Image;

  function new( int unsigned width = 2592, height = 1944 );
    this.width = width;
    this.height = height;

    data = new[this.width * this.height];
  endfunction

  function automatic shortint unsigned getPixel( int unsigned i, j );
  int unsigned address;
    begin
      address = i * this.height + j;
      if( (i >= this.height) || (j >= this.width) )
        $error("Image: out of range, i == %d, j == %d", i, j);
      return data[address];
    end
  endfunction

  function automatic int unsigned getWidth();
    return this.width;
  endfunction

  function automatic int unsigned getHeight();
    return this.height;
  endfunction

  function automatic void gen_image();
    foreach( data[i] )
      data[i] = $urandom();

    //if( !randomize() )
    //  $error("Can not randomize!");
  endfunction

endclass

`endif
