`ifndef __IMAGE_SVH
`define __IMAGE_SVH

virtual class Image#(
);

  protected int unsigned width;
  protected int unsigned height;

  rand shortint unsigned data[];

  pure virtual function automatic shortint unsigned getPixel( int unsigned i, j );

  pure virtual function automatic int unsigned getWidth();

  pure virtual function automatic int unsigned getHeight();

  pure virtual function automatic void gen_image();

endclass

`endif
