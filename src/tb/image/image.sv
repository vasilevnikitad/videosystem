virtual class Image#(
);
  local width;
  local height;

  rand shortint unsigned data[];

  pure virtual function automatic shortint unsigned getPixel( unsigned int i, j );

  pure virtual function automatic unsigned int getWidth();

  pure virtual function automatic unsigned int getHeight();

  pure virtual function automatic void gen_image();

endclass
