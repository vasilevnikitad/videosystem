`ifndef __IMAGESENSOR_SV_
`define __IMAGESENSOR_SV_


`include "image.svh"

class ImageSensor #(
);
  semaphore                       sem;
  event                           clk_ev;

  virtual dvp_if.src              dvp_src;

  // localparam TIME_2_TP = 5688;
  // localparam TIME_3_TP = 48276;
  // localparam TIME_5_TP = 14544;
  // localparam TIME_6_TP = IMAGE_WIDTH;
  // localparam TIME_7_TP = 252;
  //localparam TIME_2_TP = 568;
  //localparam TIME_3_TP = 48;
  //localparam TIME_5_TP = 145;
  //localparam TIME_6_TP = 2592; // not in use
  //localparam TIME_7_TP = 252;

  localparam TIME_2_TP = 56;
  localparam TIME_3_TP = 4;
  localparam TIME_5_TP = 14;
  localparam TIME_6_TP = 2592; // not in use
  localparam TIME_7_TP = 5;

  function new( event clk_ev, virtual dvp_if.src dvp_src );
    this.dvp_src = dvp_src;
    this.clk_ev  = clk_ev;
    this.sem     = new(1);
    this.dvp_src.data  = 0;
    this.dvp_src.href  = 0;
    this.dvp_src.vsync = 0;

  endfunction


  task automatic gen_clk(ref logic clk_o);
    clk_o = 0;
    forever
    begin
      clk_o = !clk_o;
      @clk_ev;
    end
  endtask

  //TODO: its working only with 2 byte image format!
  task automatic send_image( Image img );
    sem.get(1);
    dvp_src.vsync = 1;
    repeat (TIME_2_TP) @( clk_ev );
    dvp_src.vsync = 0;
    repeat (TIME_3_TP) @( clk_ev );

    for( int unsigned i = 0; i < img.getHeight(); ++i )
    begin
      dvp_src.href = 1;
      for( int unsigned j = 0; j < img.getWidth(); ++j )
      begin
        shortint unsigned pxl;
        pxl = img.getPixel( i, j );
        dvp_src.data = byte'( pxl );
        @clk_ev;
        dvp_src.data = byte'( pxl >> $bits(byte) );
        @clk_ev;
      end
      dvp_src.href = 0;
      repeat (TIME_7_TP) @( clk_ev );
    end
    repeat (TIME_5_TP - TIME_7_TP) @( clk_ev );
    sem.put(1);
  endtask

endclass

`endif
