`ifndef __DMA_SV_
`define __DMA_SV_

class Dma #(
  parameter AST_DATA_WIDTH    = 256,
  parameter AST_CHANNEL_WIDTH = 256
);

semaphore                sem;
event                    clk_ev;
virtual avalon_st_if#(
  .DATA_WIDTH        ( AST_DATA_WIDTH    ),
  .CHANNEL_WIDTH     ( AST_CHANNEL_WIDTH )
).snk snk_if;

int unsigned packet_size = 0;

function new(
  event clk_ev,
  virtual avalon_st_if#(
    .DATA_WIDTH        ( AST_DATA_WIDTH    ),
    .CHANNEL_WIDTH     ( AST_CHANNEL_WIDTH )
  ).snk snk
);
  this.clk_ev       = clk_ev;
  this.snk_if       = snk;
  this.snk_if.ready = 0;
  sem = new(1);
endfunction

task automatic receive_packet( ref logic [$bits(shortint)-1:0] mem[] );
  sem.get(1);
  // waiting for start of packet
  do @clk_ev;
  while( !snk_if.ready && !(snk_if.valid && snk_if.sop) );

  packet_size = 0;
  // receiving until end of packet
  for ( logic eop = 0; eop != 1; )
    begin
      if( snk_if.valid && snk_if.ready )
      begin
        if( packet_size == $size(mem) )
          $error("packet is more than a size of the buffer!");
        mem[++packet_size] = snk_if.data;
        eop                = snk_if.eop;
      end
      @clk_ev;
    end
  sem.put(1);
endtask

function automatic int unsigned getPacketSize();
  return packet_size;
endfunction

endclass

`endif
