vlib work
#
# tb:
vlog -sv ../../../if/avalon_st_if.sv
vlog -sv ../../../if/dvp_if.sv
vlog -sv ../../../tb/driver.sv

# rtl:
vlog -sv ../../../rtl/dvp.sv

# TOP:
vlog -sv ../top_tb.sv

vlog -work work -refresh
vsim -L work -novopt top_tb


add wave -noupdate -divider { DVP INTERFACE }
add wave -noupdate -radix unsigned /top_tb/dvp_if0/*
add wave -noupdate -divider { DVP } 
add wave -noupdate -radix unsigned /top_tb/dut/buff
add wave -noupdate -radix unsigned /top_tb/dut/*
add wave -noupdate -divider { AVALON STREAM INTERFACE }
add wave -noupdate -radix unsigned /top_tb/avst/*
add wave -noupdate -divider { TOP }
add wave -noupdate -radix unsigned /top_tb/*
add wave -noupdate -divider { Driver }
add wave -noupdate -radix unsigned /top_tb/driver
run -all
