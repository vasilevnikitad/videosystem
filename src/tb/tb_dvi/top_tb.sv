`timescale 10ns/1ps

`include "../driver.sv"

module top_tb;

  localparam IMAGE_WIDTH       = 2592;
  localparam IMAGE_HEIGHT      = 1944;


  localparam PCLK_FREQ_MHZ     = 96;
  localparam DVI_DATA_WIDTH    = 8;
  localparam AST_DATA_WIDTH    = 256;
  localparam AST_CHANNEL_WIDTH = $ceil( $clog2( IMAGE_HEIGHT  + 1) );

  localparam SYMBOLS_PER_BIT   = ( AST_DATA_WIDTH / DVI_DATA_WIDTH );

  localparam TIME_2_TP      = 568;
  localparam TIME_3_TP      = 48;
  localparam TIME_5_TP      = 145;
  localparam TIME_6_TP      = IMAGE_WIDTH;
  localparam TIME_7_TP      = 252;


  logic                           refclk;

  logic                           clk;
  logic                           enable;
  logic                           reset;
  Driver#(
    .AST_DATA_WIDTH               ( AST_DATA_WIDTH    ),
    .AST_CHANNEL_WIDTH            ( AST_CHANNEL_WIDTH ),
    .SYMBOLS_PER_BIT              ( SYMBOLS_PER_BIT   ),
    .DVI_DATA_WIDTH               ( DVI_DATA_WIDTH    )
  ) driver;


  avalon_st_if#(
    .DATA_WIDTH                       ( AST_DATA_WIDTH    ),
    .CHANNEL_WIDTH                    ( AST_CHANNEL_WIDTH ),
    .SYMBOLS_PER_BIT                  ( SYMBOLS_PER_BIT   )
  ) avst();

  dvp_if#(
    .DATA_WIDTH                       ( DVI_DATA_WIDTH    )
  ) dvp_if0();


//************************     MAIN CLK      ***********************************
//******************************************************************************
initial
begin
  refclk = 0;
  forever
    #( 10/2 ) refclk = !refclk;
end

assign clk = refclk;

//************************ TASKS && FUNCTIONS  *********************************
//******************************************************************************

task automatic dut_reset (
);
  reset = 0;
  repeat (1) @( posedge clk );
  reset = 1;
  repeat (1) @( posedge clk );
  reset = 0;
endtask

task automatic dut_enable (
);
  enable = 0;
  repeat (1) @( posedge clk );
  enable = 1;
endtask

task automatic sendAndGetData(
);
  fork
    driver.send_data();
    driver.get_data();
  join
  if( driver.comare_data() )
    $display("%m: data recieved correctly!");
  else begin
    $error("%m: data corrupt!");
    driver.show_height();
    driver.show_width();
    $stop;
  end
endtask

task automatic run(
  int unsigned count = 1
);
  dut_reset();
  dut_enable();
  repeat(10) @( posedge clk );
  for (int unsigned i = 0; i < count; ++i)
  begin
    driver.rand_height();
    driver.rand_width();
    driver.rand_data();
    sendAndGetData();
  end
  driver.free_data();
  $finish;
endtask

event clk_ev;
always_ff @( posedge refclk ) -> clk_ev;

//************************ DESIGN UNDER TEST ***********************************
//******************************************************************************

  dvp#(
    .DVP_DATA_WIDTH                   ( DVI_DATA_WIDTH    ),
    .A_ST_DATA_WIDTH                  ( AST_DATA_WIDTH    ),
    .A_ST_CHANNEL_WIDTH               ( AST_CHANNEL_WIDTH )
  ) dut (
    .srst_i                           ( reset             ),
    .enable_i                         ( enable            ),
    .clk_i                            ( clk               ),

    .dvp_snk                          ( dvp_if0.snk       ),

    .src                              ( avst.src          )
  );

//***************************** MAIN THREAD ************************************
//******************************************************************************
initial
begin
  driver = new( clk_ev, dvp_if0.src, avst.snk, IMAGE_HEIGHT, IMAGE_WIDTH);
  repeat(10) @( posedge clk );
  run(2);
end

endmodule
