class Driver #(
  parameter AST_DATA_WIDTH    = 256,
  parameter AST_CHANNEL_WIDTH = 8,
  parameter SYMBOLS_PER_BIT   = 32,
  parameter DVI_DATA_WIDTH    = 8
);

  typedef logic [7:0] lbyte_t;
  semaphore                       sem;
  event                           clk_ev;

  lbyte_t                         data_i[][];
  lbyte_t                         data_o[][];

  virtual dvp_if#(
    .DATA_WIDTH                   ( DVI_DATA_WIDTH    )
  ).src                           dvp_src;

  virtual avalon_st_if#(
    .DATA_WIDTH                   ( AST_DATA_WIDTH    ),
    .CHANNEL_WIDTH                ( AST_CHANNEL_WIDTH ),
    .SYMBOLS_PER_BIT              ( SYMBOLS_PER_BIT   )
  ).snk                           ast_snk;

  int     unsigned                width;
  int     unsigned                height;

  localparam TIME_2_TP = 56;
  localparam TIME_3_TP = 4;
  localparam TIME_5_TP = 14;
  localparam TIME_6_TP = 2592; // not in use
  localparam TIME_7_TP = 5;

  function new(
    event                     clk_ev,
    virtual dvp_if.src        dvp_src,
    virtual avalon_st_if#(
      .DATA_WIDTH                   ( AST_DATA_WIDTH    ),
      .CHANNEL_WIDTH                ( AST_CHANNEL_WIDTH ),
      .SYMBOLS_PER_BIT              ( SYMBOLS_PER_BIT   )
    ).snk                     snk,
    int unsigned              height,
    int unsigned              width
  );
    this.dvp_src       = dvp_src;
    this.clk_ev        = clk_ev;
    this.ast_snk       = snk;
    this.sem           = new(1);

    this.dvp_src.data  = 0;
    this.dvp_src.href  = 0;
    this.dvp_src.vsync = 0;

    this.height = height;
    this.width  = width;

    this.data_init(this.width, this.height);

  endfunction

  function automatic void free_data();
    foreach( data_i[i] )
      data_i[i].delete();

    foreach( data_o[i] )
      data_o[i].delete();
  endfunction

  function automatic void data_init(int unsigned width, int unsigned height);

    this.free_data();

    data_i = new[height];
    data_o = new[height];

    foreach( data_i[i] )
      data_i[i] = new[this.width];

    foreach( data_o[i] )
      data_o[i] = new[data_i[i].size()];

  endfunction

  function automatic void rand_data();
    foreach( data_i[i,j] )
      data_i[i][j] = $urandom();
  endfunction

  function automatic void rand_height();
    this.height = $urandom_range(1, 1944);
    $display("%m: this.height = %d", this.height);
    this.data_init(this.width, this.height);
  endfunction

  function automatic void rand_width();
    this.width = $urandom_range(1, 2592);
    $display("%m: this.width = %d", this.width);
    this.data_init(this.width, this.height);
  endfunction

  function automatic void set_height( int unsigned height );
    this.height = height;
    this.show_height();
    this.data_init(this.width, this.height);
  endfunction

  function automatic void set_width( int unsigned width );
    this.width = width;
    this.show_width();
    this.data_init(this.width, this.height);
  endfunction

  task automatic send_data();
    sem.get(1);
    dvp_src.vsync = 1;
    repeat (TIME_2_TP) @( clk_ev );
    dvp_src.vsync = 0;
    repeat (TIME_3_TP) @( clk_ev );

    $display("%m: data_i.size = %d", data_i.size);
    $display("%m: data_i[0].size = %d", data_i[0].size);
    for( int unsigned i = 0; i < data_i.size(); ++i )
    begin
      dvp_src.href = 1;
      for( int unsigned j = 0; j < data_i[i].size(); ++j )
      begin
        dvp_src.data = data_i[i][j];
        @clk_ev;
      end
      dvp_src.href = 0;
      repeat (TIME_7_TP) @( clk_ev );
    end
    repeat (TIME_5_TP - TIME_7_TP) @( clk_ev );
    sem.put(1);
    $display("data transmit!");
  endtask

  task automatic get_packet(
    ref    lbyte_t  data[],
    output int unsigned channel
  );
    int unsigned valid_width;
    int unsigned i = 0;
    localparam WIDTH = AST_DATA_WIDTH/$bits(byte);

    do @clk_ev;
    while( ast_snk.valid && ast_snk.sop );

    for( byte eop = 0; eop == 0; eop = eop )
    begin
      if( ast_snk.valid )
      begin
        if( ast_snk.eop )
        begin
          eop = 1;
          valid_width = AST_DATA_WIDTH - ast_snk.empty;
        end
        else
          valid_width = AST_DATA_WIDTH;

        for( int unsigned j = 0; j < WIDTH; ++j )
        begin
          const int shift = AST_DATA_WIDTH - $bits(byte)*(j+1);
          data[i*WIDTH + j] = ( ast_snk.data >> shift );
        end

        channel = ast_snk.channel;
        ++i;
      end
      @clk_ev;
    end
  endtask

  task automatic get_data(
  );

    lbyte_t        data[];
    int   unsigned channel;
    int   unsigned size = 0;

    foreach( data_o[i,j] )
      if( size < data_o[i].size() )
        size = data_o[i].size();

    data = new[size];

    for( int unsigned i = 0; i < data_i.size();  )
    begin
      get_packet(data, channel);
      if( channel != 0 )
      begin
        data_o[i++] = data;
      end
    end
    data.delete();
    $display("data recieved!");
  endtask

  function automatic void show_height();
    $display("%m: this.height = %d", this.height);
  endfunction

  function automatic void show_width();
    $display("%m: this.width = %d", this.width);
  endfunction

  function automatic byte comare_data();
    return ( data_i == data_o );
  endfunction

endclass
