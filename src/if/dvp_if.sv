`ifndef __DVP_IF_SV_
`define __DVP_IF_SV_

interface dvp_if #(
  parameter DATA_WIDTH = 8
) (
);

  logic                   pclk;
  logic                   vsync;
  logic                   href;
  logic [DATA_WIDTH-1:0]  data;

  modport src (
    output vsync,
    output href,
    output data,
    output pclk
  );

  modport snk (
    input vsync,
    input href,
    input data,
    input pclk
  );

endinterface

`endif
