`ifndef AVALON_MM_IF_SV
`define AVALON_MM_IF_SV
/*
    .sdram0_address                // sdram0.address
        .sdram0_burstcount         //       .burstcount
        .sdram0_waitrequest        //       .waitrequest
        .sdram0_readdata           //       .readdata
        .sdram0_readdatavalid      //       .readdatavalid
        .sdram0_read               //       .read
        .sdram0_writedata          //       .writedata
        .sdram0_byteenable         //       .byteenable
        .sdram0_write              //       .sdram0_write
*/
interface avalon_mm_if #(
  DATA_WIDTH  = 256,
  ADDR_WIDTH  = 32,
  BURST_WIDTH = 0
)
(
  // ...
);

localparam BYTE_SIZE = 8;
localparam BE_WIDTH  = DATA_WIDTH/BYTE_SIZE;

logic [ADDR_WIDTH-1:0] address;
logic                  burstcount;
logic                  waitrequest;
logic                  readdatavalid;
logic                  read;
logic [DATA_WIDTH-1:0] readdata;
logic [DATA_WIDTH-1:0] writedata;
logic [BE_WIDTH-1:0]   byteenable;
logic                  write;
logic                  debugaccess;



modport mstr (
  output               address,
  output               byteenable,
  output               read,
  output               write,
  output               burstcount,
  output               writedata,
  output               debugaccess,

  input                readdata,
  input                waitrequest,
  input                readdatavalid
);

modport slv (
  input                address,
  input                byteenable,
  input                read,
  input                write,
  input                burstcount,
  input                writedata,
  input                debugaccess,

  output               readdata,
  output               waitrequest,
  output               readdatavalid
);


endinterface

`endif
