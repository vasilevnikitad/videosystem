`ifndef AVALON_ST_IF_SV
`define AVALON_ST_IF_SV

interface avalon_st_if #(
  parameter DATA_WIDTH    = 8,
  parameter ERROR_WIDTH   = 8,
  parameter CHANNEL_WIDTH = 6,
  parameter SYMBOLS_PER_BIT = 256/8
) (
  // ...
);

localparam EMPTY_WIDTH = $ceil( $clog2( SYMBOLS_PER_BIT) );

logic [DATA_WIDTH-1:0]    data;
logic                     valid;
logic [ERROR_WIDTH-1:0]   error;
logic [CHANNEL_WIDTH-1:0] channel;
logic                     ready;
logic                     sop;
logic                     eop;
logic [EMPTY_WIDTH-1:0]   empty;

modport src ( input ready,
                output data, valid, error, channel, sop, eop, empty);

modport snk ( output ready,
                input data, valid, error, channel, sop, eop, empty);

endinterface

`endif
