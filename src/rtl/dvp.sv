// ignoring ready signal
module dvp#(
  parameter DVP_DATA_WIDTH     = 8,
  parameter A_ST_DATA_WIDTH    = 256,
  parameter A_ST_CHANNEL_WIDTH = 8,
  parameter A_ST_SYMBOL_WIDTH  = 8
) (
  // <->csr
  input                           srst_i,
  input                           enable_i,
  input                           clk_i,

  dvp_if.snk                      dvp_snk,

  // ->dma
  avalon_st_if.src                src
);


localparam BUFF_CNT_WIDTH = $ceil( $clog2( A_ST_DATA_WIDTH/DVP_DATA_WIDTH ) ) + 1;
localparam BUFF_MAX       = $ceil( A_ST_DATA_WIDTH/DVP_DATA_WIDTH );
localparam BUFF_SIZE      = A_ST_DATA_WIDTH / A_ST_SYMBOL_WIDTH;

logic [A_ST_SYMBOL_WIDTH-1:0]    buff[BUFF_SIZE-1:0];
logic [BUFF_CNT_WIDTH-1:0]       buff_cnt, buff_cnt_next; //TODO: rename counter. It counts back
logic [A_ST_DATA_WIDTH-1:0]      ast_data_next;

logic                            buff_full;

logic [DVP_DATA_WIDTH-1:0]       data;
logic                            href;
logic                            vsync;
logic                            sop_buff, sop_buff_next;
logic                            eop_buff, eop_buff_next;

logic [A_ST_CHANNEL_WIDTH-1:0]   channel_next;

logic vsync_sop, vsync_eop;
logic href_sop,  href_eop;


/* vsync_sop vsync_eop block */
always_ff @( posedge clk_i )
  if( srst_i )
  begin
    vsync_sop <= 0;
    vsync_eop <= 0;
  end
  else
  begin
    vsync_sop <= ( !vsync &&  dvp_snk.vsync );
    vsync_eop <= (  vsync && !dvp_snk.vsync );
  end

/* href_sop href_eop block */
always_ff @( posedge clk_i )
  if( srst_i )
  begin
    href_sop <= 0;
    href_eop <= 0;
  end
  else
  begin
    href_sop <= ( !href  &&  dvp_snk.href );
    href_eop <= (  href  && !dvp_snk.href );
  end

/* buff_full */
always_ff @( posedge clk_i )
  if( srst_i )
    buff_full <= 0;
  else
    buff_full <= ( buff_cnt_next == 0 );

/* buff_cnt_next block */
always_comb
begin
  buff_cnt_next = buff_cnt;
  if( href )
  begin
    if( buff_full )
      buff_cnt_next = BUFF_MAX - 1;
    else
      buff_cnt_next = (buff_cnt == 0) ? 0 : buff_cnt - 1;
  end
  else if( src.valid )
    buff_cnt_next = BUFF_MAX;
end

/* buff_cnt block */
always_ff @( posedge clk_i )
  if( srst_i )
    buff_cnt <= BUFF_MAX;
  else
    buff_cnt <= buff_cnt_next;

/* buff block */
always_ff @( posedge clk_i )
  if( href )
    buff[buff_cnt_next] = data;

/* sop_buff and eop_buff block */
always_ff @( posedge clk_i )
  if( srst_i )
  begin
    sop_buff <= 0;
    eop_buff <= 0;
  end
  else
  begin
    sop_buff <= sop_buff_next;
    eop_buff <= eop_buff_next;
  end

/* sop_buff_next block */
always_comb
begin
  sop_buff_next = sop_buff;
  if( href_sop )
    sop_buff_next = 1;
  else if( src.valid )
    sop_buff_next = 0;
end

/* eop_buff_next block */
always_comb
begin
  eop_buff_next = eop_buff;
  if( href_eop )
    eop_buff_next = 1;
  else if( src.valid )
    eop_buff_next = 0;
end

/* input latch block */
always_ff @( posedge clk_i )
  if( srst_i )
  begin
    href  <= 0;
    vsync <= 0;
  end else
  begin
    data  <= dvp_snk.data;
    href  <= dvp_snk.href;
    vsync <= dvp_snk.vsync;
  end

/* channel_next block */
always_comb
begin
  channel_next = src.channel;
  if( vsync_sop )
    channel_next = 0;
  else if ( href_sop )
    channel_next = src.channel + 1;
end

genvar i;
generate
begin
  localparam width = A_ST_SYMBOL_WIDTH;
  for( i = 0; i < $size(buff); ++i )
    begin : pack_array
      assign ast_data_next[width * i +: width] = buff[i];
    end
end
endgenerate


/* avalon_st block */
always_ff @( posedge clk_i )
  if ( srst_i )
  begin
    src.valid   <= 0;
    src.empty   <= 0;
    src.sop     <= 0;
    src.eop     <= 0;
    src.channel <= 0;
  end
  else
  begin
    src.valid   <= ( buff_full || href_eop ) || ( vsync_sop || vsync_eop );
    src.sop     <= sop_buff || vsync_sop;
    src.eop     <= href_eop || vsync_eop;
    src.data    <= ast_data_next;
    src.channel <= channel_next;
    src.empty   <= ( href_eop ) ? buff_cnt : 0;
  end

endmodule
